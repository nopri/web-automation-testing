package automationFramework;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CheckBxRadioBtn {
	public static void main(String[] args) throws InterruptedException{
		{
		System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\Geckodriver\\geckodriver.exe");
		// Create a new instance of to FireFox driver
		WebDriver wd = new FirefoxDriver();
		
		// Put on Implicit wait
		wd.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Launch the URL
		wd.get("http://medium.com");
		
		// Step3 : select the deselected Radio Button (female) for category Sex (Use IsSelected method)
		// Storing all the elements under category 'Sex' in the list of WebElements
		List<WebElement> rdBtn_Sex = wd.findElements(By.name("sex"));
		
		
		// Create a Boolean variable which will hold the value (True/False)
		boolean bValue = false;
		
		// This will check that return True, in case of first Radio Button is selected
		bValue = rdBtn_Sex.get(0).isSelected();
		
		// This will check if the bValue is True means if the first radio button is selected
		if(bValue==true){
			rdBtn_Sex.get(1).click();
		}else{
			rdBtn_Sex.get(0).click();
		}
		
		// Step 4 : Select the Third radio button for category 'Years of Exp' (Use Id attribute to select Radio Button
		WebElement rdBtn_Exp = wd.findElement(By.id("exp-2"));
		rdBtn_Exp.click();
		
		// Step 5 : Check the Check Box 'Automation Tester' for category 'Profession' (Use value attribute to
		// Find the check box or radio button element by Name
		List<WebElement> chkBx_Profession = wd.findElements(By.name("profession"));
		
		int iSize = chkBx_Profession.size();
		
		for(int i=0; i<iSize; i++){
			String sValue = chkBx_Profession.get(i).getAttribute("value");
			
			if(sValue.equalsIgnoreCase("Automation Tester")){
				chkBx_Profession.get(i).click();
				break;
			}
		}
		
		// Step 6 : Check the Check Box 'Selenium IDE' for category 'Automation Tools' (Use cssSelector)
		WebElement oCheckBox = wd.findElement(By.cssSelector("input[value='Selenium IDE']"));
		oCheckBox.click();
		
		wd.quit();
		
}
}
	
	
}