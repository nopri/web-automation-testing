package stepDefenition;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SmokeTest {
	WebDriver driver;

// Scenario 1 
@Given("^open firefox and start application$")
public void open_firefox_and_start_application() throws Throwable {
	System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\Geckodriver\\geckodriver.exe");
	driver = new FirefoxDriver();
	driver.manage().window().maximize();
	driver.get("https://ecourse.del.ac.id/login/index.php");
}

@When("^I enter valid username and valid password$")
public void I_enter_valid_username_and_valid_password() throws Throwable {
	driver.findElement(By.id("username")).sendKeys("if317011");
	driver.findElement(By.id("password")).sendKeys("nopri2000");
}

@Then("^I can login successfully$")
public void I_can_login_successfully() throws Throwable {
	  driver.findElement(By.id("loginbtn")).click();
}

// Scenario 2
@Then("^I can search course$")
public void I_can_search_course() throws Throwable {
	  driver.findElement(By.id("coursesearchbox")).click();
}

}